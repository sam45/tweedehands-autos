﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Providers.Entities;
using Microsoft.Ajax.Utilities;

namespace API.Models {

    /// <summary>
    /// Class with all important info about an advertisement and it's car
    /// </summary>
    public class Advertisement {
        public int id { get; set; }
        public DateTime lastEdited { get; set; }
        public string name { get; set; }
        public string carName { get; set; }
        public int price { get; set; }
        public string fuel { get; set; }
        public string transmission { get; set; }
        public string doors { get; set; }
        public string color { get; set; }
        public int? cilinder_capacity { get; set; }
        public int? horsepower { get; set; }
        public int mileage { get; set; }
        public int year { get; set; }
        public string options { get; set; }
        public string description { get; set; }
        public string seller_name { get; set; }
        public string thumbnail { get; set; }
        public string brand { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="book"></param>
        public Advertisement() { }

        /// <summary>
        /// Non-default constructor
        /// </summary>
        /// <param name="book"></param>
        public Advertisement(Project2_advertisement adv, Project2_car car, Project2_user user) {
            this.id = adv.id;
            this.name = adv.name;
            this.carName = car.name;
            this.price = adv.price;
            this.fuel = car.fuel;
            this.transmission = car.transmission;
            this.doors = car.doors.ToString();
            this.color = car.color;
            this.cilinder_capacity = car.cilinder_capacity;
            this.horsepower = car.horsepower;
            this.mileage = car.mileage;
            this.year = car.year.Year;
            this.options = car.options;
            this.description = car.description;
            this.seller_name = user.name;
            this.thumbnail = adv.GetThumbnail(adv.id);
            this.lastEdited = adv.last_edited;
            this.brand = car.brand;
        }
    }

    public partial class Project2_advertisement {

        private static AdvertisementsDataClassesDataContext db;

        /// <summary>
        /// Get all advertisements
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Advertisement> GetAdvertisements() {
            db = new AdvertisementsDataClassesDataContext();

            IEnumerable<Advertisement> advertisements = (from adv in db.Project2_advertisements
                                                         join car in db.Project2_cars on adv.car equals car.id
                                                         join usr in db.Project2_users on adv.user_id equals usr.id
                                                         select new Advertisement(adv, car, usr)).ToList();

            return advertisements;
        }

        /// <summary>
        /// Get the thumbnail
        /// The first photo is always the thumbnail, the id of the advertisement is the folder
        /// </summary>
        /// <returns></returns>
        public string GetThumbnail(int advId) {
            db = new AdvertisementsDataClassesDataContext();

            Project2_photo photo = db.Project2_photos.Where(a => a.advertisement == this.id).FirstOrDefault();
            if (photo != null) {
                return advId + "/" + photo.name;
            } else {
                // return dummy if there is no thumbnail image
                return "dummy.jpg";
            }
        }

        /// <summary>
        /// Get advertisements by search
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IEnumerable<Advertisement> GetAdvertisementsFiltered(string query) {
            db = new AdvertisementsDataClassesDataContext();
            string search = query.ToLower();

            return (from adv in db.Project2_advertisements
                    join car in db.Project2_cars on adv.car equals car.id
                    join usr in db.Project2_users on adv.user_id equals usr.id
                    where (adv.name.ToLower().Contains(search)
                           || car.brand.ToLower().Contains(search)
                           || car.name.ToLower().Contains(search)
                           || car.options.ToString().ToLower().Contains(search))
                    select new Advertisement(adv, car, usr)).ToList();
        }
    }
}