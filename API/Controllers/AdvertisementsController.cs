﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models;

namespace API.Controllers {
    public class AdvertisementsController : ApiController {

        //
        // GET: /api/Advertisements/
        /// <summary>
        /// Get all advertisements and the cars that are for sale
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Advertisement> GetAllAdvertisements() {
            return Project2_advertisement.GetAdvertisements();
        }

        //
        // GET: /api/Advertisements/
        /// <summary>
        ///  Get all advertisements by search
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Advertisement> GetAdvertisementsBySearch(string search) {
            return Project2_advertisement.GetAdvertisementsFiltered(search);
        }     
    }
}