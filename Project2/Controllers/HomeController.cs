﻿using System.Web.Mvc;

namespace Project2.Controllers {
    public class HomeController : BaseController {

        //
        // GET: /Home/
        public ActionResult Index() {
            ViewBag.PageId = "home";
            return View();
        }

        //
        // GET: /Home/Contact
        public ActionResult Contact() {
            ViewBag.PageId = "contact";
            return View();
        }
    }
}