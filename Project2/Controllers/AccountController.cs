﻿using Project2.Models;
using Project2.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace Project2.Controllers {
    public class AccountController : BaseController {

        //
        // GET: /Account/
        public ActionResult Index() {
            if (!Request.IsAuthenticated) return RedirectToAction("Index", "Login");
            ViewBag.PageId = "myaccount";

            Project2_user user = Project2_user.GetUserById(Convert.ToInt32(User.Identity.Name));
            List<Project2_advertisement> advertisements = user.GetAdvertisements();
            if (advertisements.Count > 0) {
                ViewBag.Advertisements = advertisements;
            }

            return View();
        }

        //
        // GET: /Account/New/
        public ActionResult New() {
            if (!Request.IsAuthenticated) return RedirectToAction("Index", "Login");
            ViewBag.PageId = "myaccount";

            return View();
        }

        //
        // POST: /Account/New/
        [HttpPost]
        public ActionResult New(AdvertisementModel model, IEnumerable<HttpPostedFileBase> files) {
            if (!Request.IsAuthenticated) return RedirectToAction("Index", "Login");
            ViewBag.PageId = "myaccount";

            if (ModelState.IsValid) {
                Project2_user user = Project2_user.GetUserById(Convert.ToInt32(User.Identity.Name));

                // put the file names in a list and check if the uploaded file is an image
                List<string> filenames = new List<string>();
                foreach (var file in files) {
                    if (file != null && Helper.IsImage(file.FileName)) {
                        filenames.Add(file.FileName);
                    }
                }

                // add the advertisement and save the id (to use it for the name of the folder of the images)
                int id = Project2_advertisement.InsertAdvertisement(model, filenames, user.id);
                // create the directory for the specific advertisement id
                Directory.CreateDirectory(Path.Combine(Server.MapPath("~/Content/img/"), id.ToString()));

                // save all files
                foreach (var file in files) {
                    if (file != null) {
                        string fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Content/img/" + id.ToString() + "/"), fileName);
                        file.SaveAs(path);
                    }
                }

                return RedirectToAction("Index", "Account");
            }

            return View(model);
        }

        //
        // GET: /Account/Edit/{id}
        public ActionResult Edit(int id) {
            if (!Request.IsAuthenticated) return RedirectToAction("Index", "Login");
            ViewBag.PageId = "myaccount";

            Project2_user user = Project2_user.GetUserById(Convert.ToInt32(User.Identity.Name));
            Project2_advertisement advertisement = user.GetAdvertisement(id);

            // redirect to overview if the advertisement doesn't exist
            if (advertisement == null) return RedirectToAction("Index");
            ViewBag.Advertisement = advertisement;

            // Create the model
            AdvertisementModel model = new AdvertisementModel(advertisement, advertisement.GetCar());

            return View(model);
        }

        //
        // POST: /Account/Edit/{id}
        [HttpPost]
        public ActionResult Edit(AdvertisementModel model, int id, IEnumerable<HttpPostedFileBase> files) {
            if (!Request.IsAuthenticated) return RedirectToAction("Index", "Login");
            ViewBag.PageId = "myaccount";

            Project2_user user = Project2_user.GetUserById(Convert.ToInt32(User.Identity.Name));
            Project2_advertisement advertisement = user.GetAdvertisement(id);

            // redirect to overview if the advertisement doesn't exist
            if (advertisement == null) return RedirectToAction("Index");

            if (ModelState.IsValid) {
                // put the file names in a list and check if the uploaded file is an image
                List<string> filenames = new List<string>();
                foreach (var file in files) {
                    if (file != null && Helper.IsImage(file.FileName)) {
                        filenames.Add(file.FileName);
                    }
                }

                var test = Server.MapPath("/");
 

                // update the advertisement
                advertisement.UpdateAdvertisement(model, user.id, filenames);

                // add new images
                if (filenames.Count > 0) {

                    // save all files
                    // localhost
                    if (Request.IsLocal) {
                        foreach (var file in files) {
                            if (file != null) {
                                string fileName = Path.GetFileName(file.FileName);

                                var path = Server.MapPath("/") + "Content\\img\\" + id.ToString() + "\\" + fileName;
                                Directory.CreateDirectory(Server.MapPath("/") + "Content\\img\\" + id.ToString());
                                file.SaveAs(path);
                            }
                        }
                    }

                    // on server
                    else {
                        // save all files
                        foreach (var file in files) {
                            if (file != null) {
                                string fileName = Path.GetFileName(file.FileName);
                                var path = Path.Combine(Server.MapPath("~/Content/img/" + id.ToString() + "/"), fileName);
                                file.SaveAs(path);
                            }
                        } 
                    }  
                }
                // succes
                return RedirectToAction("Index", "Account");
            }

            ViewBag.Advertisement = advertisement;
            return View(model);
        }

        //
        // GET: /Account/Delete/{id}
        public ActionResult Delete(int id) {
            if (!Request.IsAuthenticated) return RedirectToAction("Index", "Login");
            ViewBag.PageId = "myaccount";

            Project2_user user = Project2_user.GetUserById(Convert.ToInt32(User.Identity.Name));
            Project2_advertisement advertisement = user.GetAdvertisement(id);

            // redirect to overview if the advertisement doesn't exist
            if (advertisement == null) return RedirectToAction("Index");

            // delete the advertisement
            advertisement.DeleteAdvertisement();

            return RedirectToAction("Index", "Account");
        }

        //
        // GET: /Account/Info
        public ActionResult Info() {
            if (!Request.IsAuthenticated) return RedirectToAction("Index", "Login");
            ViewBag.PageId = "myaccount";

            // get the user
            Project2_user user = Project2_user.GetUserById(Convert.ToInt32(User.Identity.Name));

            // return the view with a newly created model
            return View(new Project2.Models.Project2_user.UserModel.RegisterModel(user));
        }

        //
        // POST: /Account/Info
        [HttpPost]
        public ActionResult Info(Project2_user.UserModel.RegisterModel model) {
            if (!Request.IsAuthenticated) return RedirectToAction("Index", "Login");
            ViewBag.PageId = "myaccount";

            if (ModelState.IsValid) {
                try {
                    Project2_user user = Project2_user.GetUserById(Convert.ToInt32(User.Identity.Name));
                    user.UpdateUserAccount(model);
                    return RedirectToAction("Index", "Account");

                } catch (CustomException exc) {
                    switch (exc.Code) {
                        case CustomExceptionCode.REGISTER_EMAIL_ALREADYEXISTS:
                            ModelState.AddModelError("email", "Email adres is reeds geregistreerd.");
                            break;
                        case CustomExceptionCode.REGISTER_PASSWORD_NOTSAFE:
                            ModelState.AddModelError("password", "Wachtwoord moet minstens 6 tekens zijn");
                            break;
                        case CustomExceptionCode.REGISTER_PASSWORD_NOTTHESAME:
                            ModelState.AddModelError("password", "Wachtwoorden in beide velden komen niet overeen");
                            break;
                        case CustomExceptionCode.REGISTER_POSTALCODE_NOTVALID:
                            ModelState.AddModelError("postalcode", "De postcode is ongeldig");
                            break;
                        default:
                            ModelState.AddModelError("general", "Er is iets misgelopen");
                            break;
                    }
                }
            }

            return View(model);
        }
    }
}