﻿using Project2.Models;
using Project2.Utils;
using System.Web.Mvc;
using System.Web.Security;

namespace Project2.Controllers {
    public class LoginController : BaseController {

        //
        // GET: /Login/
        public ActionResult Index() {
            ViewBag.PageId = "home";
            return View();
        }

        //
        // POST: /Login/
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Index(Project2_user.UserModel.LoginModel model, FormCollection collection) {
            ViewBag.PageId = "home";

            if (ModelState.IsValid) {
                model.email = model.email.ToLower();

                // hash the password and get the user
                Project2_user user = Project2_user.GetUserByEmailAndPassword(model.email, Helper.GenerateHashWithSalt(model.password, model.email));
                if (user != null) {
                    FormsAuthentication.SetAuthCookie(user.id.ToString(), true);
                    // logged in
                    return RedirectToAction("Index", "Account");
                } else {
                    ModelState.AddModelError("", "Het ingegeven emailadres of wachtwoord is verkeerd");
                }
            }

            return View(model);
        }

        //
        // GET: /Login/Register
        public ActionResult Register() {
            ViewBag.PageId = "home";
            return View();
        }

        //
        // POST: /Login/Register
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Register(Project2_user.UserModel.RegisterModel model, FormCollection collection) {
            ViewBag.PageId = "home";
            if (ModelState.IsValid) {
                try {
                    Project2_user user = Project2_user.CreateUserAccount(model);
                    if (user != null) {
                        FormsAuthentication.SetAuthCookie(user.id.ToString(), true);
                        // logged in
                        return RedirectToAction("Index", "Account");
                    }
                } catch (CustomException exc) {
                    switch (exc.Code) {
                        case CustomExceptionCode.REGISTER_EMAIL_ALREADYEXISTS:
                            ModelState.AddModelError("email", "Email adres is reeds geregistreerd.");
                            break;
                        case CustomExceptionCode.REGISTER_PASSWORD_NOTSAFE:
                            ModelState.AddModelError("password", "Wachtwoord moet minstens 6 tekens zijn");
                            break;
                        case CustomExceptionCode.REGISTER_PASSWORD_NOTTHESAME:
                            ModelState.AddModelError("password", "Wachtwoorden in beide velden komen niet overeen");
                            break;
                        case CustomExceptionCode.REGISTER_POSTALCODE_NOTVALID:
                            ModelState.AddModelError("postalcode", "De postcode is ongeldig");
                            break;
                        default:
                            ModelState.AddModelError("general", "Er is iets misgelopen");
                            break;
                    }
                }
            }

            return View(model);
        }

        //
        // GET: /Login/Logout
        public ActionResult Logout() {
            ViewBag.PageId = "home";

            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

    }
}
