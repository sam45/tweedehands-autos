﻿using MvcPaging;
using Project2.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Project2.Controllers {
    public class AutosController : BaseController {

        //
        // GET: /Autos/
        public ActionResult Index(FilterModel model, int? page) {
            // create a dictionary with only the filters that are used, skip the others
            Dictionary<string, string> keyvalue = new Dictionary<string, string>();
            for (int i = 0; i < Request.QueryString.Count; i++) {
                if (Request.QueryString[i] != "" && Request.QueryString.AllKeys[i] != "page") {
                    keyvalue.Add(Request.QueryString.AllKeys[i], Request.QueryString[i]);
                }
            }

            // only filter when needed
            List<Project2_advertisement> advertisements;
            if (keyvalue.Count > 1) {
                advertisements = Project2_advertisement.GetAdvertisementsByFilter(keyvalue);
                ViewBag.Filtered = true;
            } else {
                advertisements = Project2_advertisement.GetAdvertisements();
            }

            // get the current page and check if it's in range of the available items to display
            int currentPageIndex = page.HasValue ? page.Value : 1;
            // paginate
            GetPagination(currentPageIndex, advertisements);
            // get all common viewbags to prevent double code
            GetIndexViewBags();

            ViewBag.FilterMethod = "filter";
            return View(model);
        }

        //
        // POST: /Autos/
        [HttpPost]
        public ActionResult Index(FormCollection collection) {
            List<Project2_advertisement> advertisements;
            // Get the posted data from the search form
            if (collection.Get(0).Trim() != "") {
                ViewBag.SearchTerm = collection.Get(0);
                ViewBag.FilterMethod = "search";
                ViewBag.Filtered = true;
                // get only the advertisements matching the search
                advertisements = Project2_advertisement.GetAdvertisementsBySearch(collection.Get(0));

                IList<Project2_advertisement> advertisementsIList = advertisements;
                // get the current page and check if it's in range of the available items to display
                int currentPageIndex = 1;
                if (collection.Get("pageSearch") != null) {
                    currentPageIndex = Convert.ToInt32(collection.Get("pageSearch"));
                }

                // paginate
                GetPagination(currentPageIndex, advertisements);
                // get all common viewbags to prevent double code
                GetIndexViewBags();
            }

            return View();
        }

        //
        // GET: /Autos/Detail/{id}
        public ActionResult Detail(int id) {
            Project2_advertisement advertisement = Project2_advertisement.GetAdvertisement(id);

            // redirect to overview if the advertisement doesn't exist
            if (advertisement == null) return RedirectToAction("Index");
            GetDetailViewBags(advertisement);

            return View();
        }

        //
        // POST: /Autos/Detail/{id}
        [HttpPost]
        public ActionResult Detail(int id, OfferModel model) {
            Project2_advertisement advertisement = Project2_advertisement.GetAdvertisement(id);

            if (ModelState.IsValid) {
                advertisement.NewOffer(model);
                return RedirectToAction("Detail", "Autos");
            }

            // redirect to overview if the advertisement doesn't exist
            if (advertisement == null) return RedirectToAction("Index");
            // get all common viewbags to prevent double code
            GetDetailViewBags(advertisement);

            return View(model);
        }

        /// <summary>
        /// Get all common ViewBags for the Index page
        /// </summary>
        private void GetIndexViewBags() {
            ViewBag.PageId = "autos";
            ViewBag.Provinces = Project2_user.GetProvinces();
            ViewBag.Colors = Project2_car.GetColors();
            ViewBag.Fuels = Project2_car.GetFuels();
            ViewBag.Transmissions = Project2_car.GetTransmissions();
            ViewBag.Brands = Project2_car.GetBrands();
            ViewBag.Doors = Project2_car.GetDoors();
        }

        /// <summary>
        /// Get all common ViewBags for the Detail page
        /// </summary>
        private void GetDetailViewBags(Project2_advertisement advertisement) {
            ViewBag.PageId = "autos";
            ViewBag.User = advertisement.GetUser();
            List<Project2_photo> photos = advertisement.GetPhotos();
            if (photos.Count > 0) {
                ViewBag.Photos = photos;
            }
            ViewBag.advertisement = advertisement;
            ViewBag.Next = advertisement.GetNextAdvertisement();

            // only get offers when there are any
            List<Project2_offer> offers = advertisement.GetOffers();
            if (offers.Count > 0) {
                ViewBag.Offers = offers;
            }
            ViewBag.Car = advertisement.GetCar();
            List<Project2_advertisement> relatedAdvertisements = advertisement.GetRelatedAdvertisements();
            if (relatedAdvertisements.Count > 0) {
                ViewBag.RelatedAdvertisements = relatedAdvertisements;
            }
        }

        /// <summary>
        /// Get the pagination
        /// </summary>
        /// <param name="currentPageIndex"></param>
        /// <param name="advertisements"></param>
        private void GetPagination(int currentPageIndex, List<Project2_advertisement> advertisements) {
            // create an IList from the advertisements
            IList<Project2_advertisement> advertisementsIList = advertisements;
            if (currentPageIndex >
                (Math.Ceiling((double)advertisements.Count / (double)Project2_advertisement.ADVERTISEMENTS_PER_PAGE))) {
                currentPageIndex = 1;
            }

            // create the pagination
            advertisementsIList = advertisementsIList.ToPagedList(currentPageIndex,
                Project2_advertisement.ADVERTISEMENTS_PER_PAGE);
            ViewBag.Currentpage = currentPageIndex;
            ViewBag.TotalItemCount = advertisements.Count;

            if (advertisementsIList.Count > 0) {
                ViewBag.Advertisements = advertisementsIList;
            }
        }
    }
}