﻿using Project2.Models;
using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace Project2.Controllers {

    public class BaseController : Controller {

        /// <summary>
        /// Initialize logged in user
        /// </summary>
        protected override void Initialize(RequestContext requestContext) {
            base.Initialize(requestContext);

            // get the user name
            if (User.Identity.IsAuthenticated) {
                ViewBag.UserAccount = Project2_user.GetUserById(Convert.ToInt32(User.Identity.Name));
            }
        }
    }
}
