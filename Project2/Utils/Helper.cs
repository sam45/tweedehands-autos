﻿using System;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace Project2.Utils {

        public static class Helper {
            /// <summary>
            /// Truncate strings
            /// @see http://www.sitepoint.com/forums/showthread.php?391802-truncating-a-field-using-the-repeater-control
            /// </summary>
            /// <param name="description"></param>
            /// <returns></returns>
            public static string Truncate(string text, int maxLength) {
                if (text.Length > maxLength) {
                    text = text.Substring(0, maxLength);
                }
                return text + "...";
            }

            /// <summary>
            /// Computes a salted hash of the password and salt provided and returns as a base64 encoded string.
            /// <see cref="http://www.devtoolshed.com/c-generate-password-hash-salt"/>
            /// </summary>
            /// <param name="password">The password to hash.</param>
            /// <param name="salt">The salt to use in the hash.</param>
            public static string GenerateHashWithSalt(string password, string salt) {
                // merge password and salt together
                string sHashWithSalt = password + salt;
                // convert this merged value to a byte array
                byte[] saltedHashBytes = Encoding.UTF8.GetBytes(sHashWithSalt);
                // use hash algorithm to compute the hash
                System.Security.Cryptography.HashAlgorithm algorithm = new System.Security.Cryptography.SHA256Managed();
                // convert merged bytes to a hash as byte array
                byte[] hash = algorithm.ComputeHash(saltedHashBytes);
                // return the has as a base 64 encoded string
                return Convert.ToBase64String(hash);
            }

            /// <summary>
            /// Check if the uploaded file is an image
            /// </summary>
            /// <param name="base64String"></param>
            /// <returns></returns>
            public static bool IsImage(string filename) {
                string[] split = filename.Split('.');
                if (split[split.Length - 1].Equals("jpg") || split[split.Length - 1].Equals("png")) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
    }