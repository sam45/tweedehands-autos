﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Project2.Utils {
    public static class ViewHelper {

        /// <summary>
        /// Get a list of all provinces in Belgium
        /// </summary>
        /// <returns></returns>
        public static SelectList GetProvinces() {
            return new SelectList(new string[] {
                "Antwerpen", "Limburg", "Oost-Vlaanderen", "Vlaams-Brabant", "West-Vlaanderen", "Henegouwen", "Luik", "Luxemburg", "Namen", "Waals-Brabant"
            });
        }
    }
}
