﻿using System.Collections.Generic;
using System.Linq;

namespace Project2.Models {
    public partial class Project2_car {
        private static AdvertisementsDataClassesDataContext db;

        #region show

        /// <summary>
        /// Get all colors and filter out the duplicates
        /// </summary>
        /// <returns></returns>
        public static List<string> GetColors() {
            db = new AdvertisementsDataClassesDataContext();

            return db.Project2_cars.OrderBy(b => b.color).Select(a => a.color).Distinct().ToList();
        }

        /// <summary>
        /// Get all fuel types and filter out the duplicates
        /// </summary>
        /// <returns></returns>
        public static List<string> GetFuels() {
            db = new AdvertisementsDataClassesDataContext();

            return db.Project2_cars.OrderBy(b => b.fuel).Select(a => a.fuel).Distinct().ToList();
        }

        /// <summary>
        /// Get all transmission types and filter out the duplicates
        /// </summary>
        /// <returns></returns>
        public static List<string> GetTransmissions() {
            db = new AdvertisementsDataClassesDataContext();

            return db.Project2_cars.OrderBy(b => b.transmission).Select(a => a.transmission).Distinct().ToList();
        }

        /// <summary>
        /// Get all transmission types and filter out the duplicates
        /// </summary>
        /// <returns></returns>
        public static List<string> GetBrands() {
            db = new AdvertisementsDataClassesDataContext();

            return db.Project2_cars.OrderBy(b => b.brand).Select(a => a.brand).Distinct().ToList();
        }

        /// <summary>
        /// Get all door types and filter out the duplicates
        /// </summary>
        /// <returns></returns>
        public static List<int> GetDoors() {
            db = new AdvertisementsDataClassesDataContext();

            return db.Project2_cars.OrderBy(b => b.doors).Select(a => a.doors).Distinct().ToList();
        }

        #endregion
    }
}