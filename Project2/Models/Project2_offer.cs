﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Project2.Models {
    [MetadataType(typeof(OfferModel))]
    public partial class Project2_offer {
        private static AdvertisementsDataClassesDataContext db;
        public List<Project2_offer> GetOffers(int advertisementId) {
            db = new AdvertisementsDataClassesDataContext();

            return db.Project2_offers.Where(a => a.advertisement_id == advertisementId).OrderByDescending(a => a.timestamp).Take(5).ToList();
        }
    }

    /// <summary>
    /// Model for logging in
    /// </summary>
    public class OfferModel {
        [Required(ErrorMessage = "Vul uw email adres in")]
        public string email { get; set; }

        [Required(ErrorMessage = "Vul uw naam in")]
        public string name { get; set; }

        [Required(ErrorMessage = "Vul een bedrag in")]
        public string price { get; set; }
    }
}