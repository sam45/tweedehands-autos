﻿using System.ComponentModel.DataAnnotations;

namespace Project2.Models {

    /// <summary>
    /// Model that contains the data for creating a new advertisement
    /// </summary>
    public class AdvertisementModel {

        [Required(ErrorMessage = "Vul de naam van het zoekertje in")]
        [Display(Name = "Naam zoekertje*")]
        public string name { get; set; }

        [Required(ErrorMessage = "Vul het merk van de auto in")]
        [Display(Name = "Merk auto*")]
        public string brand { get; set; }

        [Required(ErrorMessage = "Vul het model in")]
        [Display(Name = "Model auto*")]
        public string carName { get; set; }

        [Required(ErrorMessage = "Vul de prijs in")]
        [RegularExpression(@"^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$", ErrorMessage = "De prijs moet een getal zijn")]
        [Display(Name = "Richtprijs auto*")]
        public int price { get; set; }

        [Required(ErrorMessage = "Vul het soort brandstof in")]
        [Display(Name = "Brandstof*")]
        public string fuel { get; set; }

        [Required(ErrorMessage = "Vul het soort transmissie in")]
        [Display(Name = "Transmissie*")]
        public string transmission { get; set; }

        [Required(ErrorMessage = "Vul het aantal deuren van de wagen in")]
        [RegularExpression(@"^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$", ErrorMessage = "Het aantal deuren moet een getal zijn")]
        [Display(Name = "Aantal deuren*")]
        public string doors { get; set; }

        [Required(ErrorMessage = "Vul de kleur van de wagen in")]
        [Display(Name = "Kleur*")]
        public string color { get; set; }

        [Display(Name = "Cilinder capaciteit (cm³)")]
        [RegularExpression(@"^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$", ErrorMessage = "De cilindercapaciteit moet een getal zijn")]
        public int? cilinder_capacity { get; set; }

        [Display(Name = "Paardenkracht (pk)")]
        [RegularExpression(@"^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$", ErrorMessage = "De paardenkracht moet een getal zijn")]
        public int? horsepower { get; set; }

        [Required(ErrorMessage = "Vul het aantal gereden km in")]
        [RegularExpression(@"^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$", ErrorMessage = "Het aantal gereden km moet een getal zijn")]
        [Display(Name = "Aantal gereden km*")]
        public int mileage { get; set; }

        [Required(ErrorMessage = "Vul de bouwmaand in")]
        [RegularExpression(@"1[0-2]|[1-9]", ErrorMessage = "De bouwmaand moet een geldige maand zijn")]
        [Display(Name = "Bouwmaand*")]
        public int month { get; set; }

        [Required(ErrorMessage = "Vul het bouwjaar in")]
        [RegularExpression(@"^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$", ErrorMessage = "Het bouwjaar moet een getal zijn")]
        [Display(Name = "Bouwjaar*")]
        public int year { get; set; }

        [Display(Name = "Opties")]
        public string options { get; set; }

        [Required(ErrorMessage = "Geef een beschrijving van de wagen")]
        [Display(Name = "Beschrijving*")]
        public string description { get; set; }

        public AdvertisementModel() { }

        public AdvertisementModel(Project2_advertisement adv, Project2_car car) {
            this.name = adv.name;
            this.carName = car.name;
            this.price = adv.price;
            this.fuel = car.fuel;
            this.transmission = car.transmission;
            this.doors = car.doors.ToString();
            this.color = car.color;
            this.cilinder_capacity = car.cilinder_capacity;
            this.horsepower = car.horsepower;
            this.mileage = car.mileage;
            this.month = car.year.Month;
            this.year = car.year.Year;
            this.options = car.options;
            this.description = car.description;
            this.brand = car.brand;
        }
    }
}