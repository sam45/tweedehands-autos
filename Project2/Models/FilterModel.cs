﻿
namespace Project2.Models {

    /// <summary>
    /// Model that contains the data for the filters on the overview page
    /// </summary>
    public class FilterModel {
        public int? priceMin { get; set; }
        public int? priceMax { get; set; }
        public int? mileageMin { get; set; }
        public int? mileageMax { get; set; }
        public int? yearMin { get; set; }
        public int? yearMax { get; set; }
        public string province { get; set; }
        public string color { get; set; }
        public string fuel { get; set; }
        public string transmission { get; set; }
        public string doors { get; set; }
        public string brand { get; set; }
    }
}