﻿using Project2.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Project2.Models {

    [MetadataType(typeof(UserModel))]
    public partial class Project2_user {
        private static AdvertisementsDataClassesDataContext db;

        #region show

        /// <summary>
        /// Get a list of all provinces
        /// </summary>
        /// <returns></returns>
        public static List<string> GetProvinces() {
            db = new AdvertisementsDataClassesDataContext();

            // only get provinces when there is an advertisement open
            return (from usr in db.Project2_users
                    join adv in db.Project2_advertisements on usr.id equals adv.user_id
                    join car in db.Project2_cars on adv.car equals car.id

                    select usr.province).Distinct().ToList();
        }

        /// <summary>
        /// Get a single user by email and password
        /// </summary>
        public static Project2_user GetUserByEmailAndPassword(string email, string password) {
            db = new AdvertisementsDataClassesDataContext();

            // return user         
            return db.Project2_users.SingleOrDefault(u => u.email == email && u.password.Equals(password));
        }

        /// <summary>
        /// Get a single user by id
        /// </summary>
        public static Project2_user GetUserById(int id) {
            db = new AdvertisementsDataClassesDataContext();

            // return user
            return db.Project2_users.SingleOrDefault(u => u.id == id);
        }

        /// <summary>
        /// Get all advertisements from the current user
        /// </summary>
        /// <returns></returns>
        public List<Project2_advertisement> GetAdvertisements() {
            db = new AdvertisementsDataClassesDataContext();

            return db.Project2_advertisements.Where(a => a.user_id == this.id).ToList();
        }

        /// <summary>
        /// Get a single advertisement
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Project2_advertisement GetAdvertisement(int id) {
            db = new AdvertisementsDataClassesDataContext();

            return db.Project2_advertisements.Where(a => a.id == id && a.user_id == this.id).SingleOrDefault();
        }

        #endregion

        #region insert

        /// <summary>
        /// Create a new account
        /// </summary>
        /// <param name="newUser"></param>
        /// <returns></returns>
        public static Project2_user CreateUserAccount(Project2.Models.Project2_user.UserModel.RegisterModel newUser) {
            db = new AdvertisementsDataClassesDataContext();

            // checks
            if (db.Project2_users.Select(a => a.email).Distinct().ToList().Contains(newUser.email.ToLower())) {
                throw new CustomException(CustomExceptionCode.REGISTER_EMAIL_ALREADYEXISTS);
            }

            if (newUser.password.Length < 6) {
                throw new CustomException(CustomExceptionCode.REGISTER_PASSWORD_NOTSAFE);
            }

            if (newUser.password != newUser.password2) {
                throw new CustomException(CustomExceptionCode.REGISTER_PASSWORD_NOTTHESAME);
            }

            if (!System.Text.RegularExpressions.Regex.IsMatch(newUser.postal_code, @"\d{4}")) {
                throw new CustomException(CustomExceptionCode.REGISTER_POSTALCODE_NOTVALID);
            }

            // create user
            Project2_user user = new Project2_user();
            user.firstname = newUser.firstname;
            user.name = newUser.name;

            user.email = newUser.email.ToLower();
            user.password = Helper.GenerateHashWithSalt(newUser.password, newUser.email.ToLower());  // hash pasword
            user.postal_code = Convert.ToInt32(newUser.postal_code);
            user.province = newUser.province;
            user.city = newUser.city;
            user.address = newUser.address;
            user.phone = newUser.phone != "" ? newUser.phone : null;

            db.Project2_users.InsertOnSubmit(user);
            db.SubmitChanges();

            return user;
        }

        #region update

        public void UpdateUserAccount(UserModel.RegisterModel model) {
            // checks
            if (model.email != this.email && db.Project2_users.Select(a => a.email).Distinct().ToList().Contains(model.email.ToLower())) {
                throw new CustomException(CustomExceptionCode.REGISTER_EMAIL_ALREADYEXISTS);
            }

            if (model.password.Length <= 6) {
                throw new CustomException(CustomExceptionCode.REGISTER_PASSWORD_NOTSAFE);
            }

            if (model.password != model.password2) {
                throw new CustomException(CustomExceptionCode.REGISTER_PASSWORD_NOTTHESAME);
            }

            if (!System.Text.RegularExpressions.Regex.IsMatch(model.postal_code, @"\d{4}")) {
                throw new CustomException(CustomExceptionCode.REGISTER_POSTALCODE_NOTVALID);
            }

            this.firstname = model.firstname;
            this.name = model.name;
            this.email = model.email.ToLower();
            this.password = Helper.GenerateHashWithSalt(model.password, model.email.ToLower());  // hash pasword
            this.postal_code = Convert.ToInt32(model.postal_code);
            this.province = model.province;
            this.city = model.city;
            this.address = model.address;
            this.phone = model.phone != "" ? model.phone : null;

            db.SubmitChanges();
        }

        #endregion

        #endregion

        /// <summary>
        /// Model for logging in
        /// </summary>
        public class UserModel {
            /// <summary>
            /// Model inloggen
            /// </summary>
            public class LoginModel {
                [Required(ErrorMessage = "Vul uw email adres in")]
                [Display(Name = "Email")]
                public string email { get; set; }

                [Required(ErrorMessage = "Vul uw wachtwoord in")]
                [DataType(DataType.Password)]
                [Display(Name = "Wachtwoord")]
                public string password { get; set; }
            }

            /// <summary>
            /// Model for registering
            /// </summary>
            public class RegisterModel {
                [Required(ErrorMessage = "Vul uw voornaam in")]
                [Display(Name = "Voornaam*")]
                public string firstname { get; set; }

                [Required(ErrorMessage = "Vul uw naam in")]
                [Display(Name = "Naam*")]
                public string name { get; set; }

                [Required(ErrorMessage = "Vul uw email adres in")]
                [Display(Name = "Email*")]
                public string email { get; set; }

                [Required(ErrorMessage = "Vul uw wachtwoord in")]
                [DataType(DataType.Password)]
                [Display(Name = "Wachtwoord*")]
                public string password { get; set; }

                [Required(ErrorMessage = "Vul uw wachtwoord nogmaals in")]
                [DataType(DataType.Password)]
                [Display(Name = "Wachtwoord*")]
                public string password2 { get; set; }

                [Display(Name = "Telefoon")]
                public string phone { get; set; }

                [Required(ErrorMessage = "Vul uw adres in")]
                [Display(Name = "Adres*")]
                public string address { get; set; }

                [Required(ErrorMessage = "Vul uw gemeente in")]
                [Display(Name = "Gemeente*")]
                public string city { get; set; }

                [Required(ErrorMessage = "Vul uw postcode in")]
                [RegularExpression(@"\d{4}", ErrorMessage = "De postcode is ongeldig")]
                [Display(Name = "Postcode*")]
                public string postal_code { get; set; }

                [Required(ErrorMessage = "Vul uw provincie in")]
                [Display(Name = "Provincie*")]
                public string province { get; set; }

                /// <summary>
                /// Default contstuctor
                /// </summary>
                public RegisterModel() { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="user"></param>
                public RegisterModel(Project2_user user) {
                    this.firstname = user.firstname;
                    this.name = user.name;
                    this.email = user.email;
                    this.phone = user.phone;
                    this.address = user.address;
                    this.city = user.city;
                    this.postal_code = user.postal_code.ToString();
                    this.province = user.province;
                } 
            }
        }
    }
}