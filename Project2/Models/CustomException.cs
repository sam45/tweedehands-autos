﻿using System;

namespace Project2.Models {
    public enum CustomExceptionCode {
        REGISTER_EMAIL_ALREADYEXISTS, REGISTER_PASSWORD_NOTTHESAME, REGISTER_POSTALCODE_NOTVALID, REGISTER_PASSWORD_NOTSAFE
    };

    public class CustomException : Exception {
        public CustomExceptionCode Code { get; set; }

        public CustomException(CustomExceptionCode code) {
            this.Code = code;
        }

        public CustomException(CustomExceptionCode code, string message)
            : base(message) {
            this.Code = code;
        }
    }
}