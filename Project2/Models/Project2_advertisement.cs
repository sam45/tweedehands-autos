﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Project2.Models {
    public partial class Project2_advertisement {
        public static readonly int ADVERTISEMENTS_PER_PAGE = 5;
        private static AdvertisementsDataClassesDataContext db;

        #region show

        /// <summary>
        /// Get all advertisements
        /// </summary>
        /// <returns></returns>
        public static List<Project2_advertisement> GetAdvertisements() {
            db = new AdvertisementsDataClassesDataContext();

            return db.Project2_advertisements.OrderByDescending(a => a.last_edited).ToList();
        }

        /// <summary>
        /// Get a single advertisement
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Project2_advertisement GetAdvertisement(int id) {
            db = new AdvertisementsDataClassesDataContext();

            return db.Project2_advertisements.Where(a => a.id == id).SingleOrDefault();
        }

        /// <summary>
        /// Get the next advertisement
        /// </summary>
        /// <returns></returns>
        public Project2_advertisement GetNextAdvertisement() {
            db = new AdvertisementsDataClassesDataContext();

            List<Project2_advertisement> advertisements =
                db.Project2_advertisements.OrderByDescending(a => a.last_edited).ToList();
            int index = advertisements.FindIndex(a => a.id == this.id);

            return
                db.Project2_advertisements.OrderByDescending(a => a.last_edited)
                    .Skip(index + 1)
                    .Take(1)
                    .SingleOrDefault();
        }

        /// <summary>
        /// Get the car from the advertisement
        /// </summary>
        /// <returns></returns>
        public Project2_car GetCar() {
            db = new AdvertisementsDataClassesDataContext();

            return db.Project2_cars.Where(a => a.id == this.car).SingleOrDefault();
        }

        // <summary>
        /// Get the user from an advertisement
        /// </summary>
        /// <returns></returns>
        public Project2_user GetUser() {
            db = new AdvertisementsDataClassesDataContext();

            return db.Project2_users.Where(a => a.id == this.user_id).SingleOrDefault();
        }

        /// <summary>
        /// Get the thumbnail
        /// The first photo is always the thumbnail, the id of the advertisement is the folder
        /// </summary>
        /// <returns></returns>
        public Project2_photo GetThumbnail() {
            db = new AdvertisementsDataClassesDataContext();

            return db.Project2_photos.Where(a => a.advertisement == this.id).FirstOrDefault();
        }

        /// <summary>
        /// Get all photo's from the advertisement
        /// </summary>
        /// <returns></returns>
        public List<Project2_photo> GetPhotos() {
            db = new AdvertisementsDataClassesDataContext();

            return db.Project2_photos.Where(a => a.advertisement == this.id).ToList();
        }

        /// <summary>
        /// Get all offers on the current advertisement
        /// </summary>
        /// <returns></returns>
        public List<Project2_offer> GetOffers() {
            db = new AdvertisementsDataClassesDataContext();

            return db.Project2_offers.Where(a => a.advertisement_id == this.id).OrderByDescending(a => a.price).Take(5).ToList();
        }

        /// <summary>
        /// Get 3 related advertisements
        /// The advertisements are taken randomly from all matching possibilities so they are/can be different on each refresh
        /// </summary>
        /// <returns></returns>
        public List<Project2_advertisement> GetRelatedAdvertisements() {
            db = new AdvertisementsDataClassesDataContext();

            List<Project2_advertisement> advertisements = (from adv in db.Project2_advertisements
                                                           where adv.Project2_car.name.ToLower().Contains(this.Project2_car.brand) && adv.id != this.id
                                                           select adv).ToList();

            return advertisements.Take(4).ToList();
        }

        /// <summary>
        /// Get the highest offer on an advertisement
        /// </summary>
        /// <returns></returns>
        public int GetHighestOffer() {
            db = new AdvertisementsDataClassesDataContext();

            List<Project2_offer> offers = db.Project2_offers.Where(a => a.advertisement_id == id).ToList();
            int price;
            if (offers.Count <= 0) {
                price = 0;
            } else {
                price = offers.Max(a => a.price);
            }
            return price;
        }

        /// <summary>
        /// Get advertisements by search (name, brand, options)
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static List<Project2_advertisement> GetAdvertisementsBySearch(string query) {
            db = new AdvertisementsDataClassesDataContext();
            string search = query.ToLower();

            return (from adv in db.Project2_advertisements
                    join car in db.Project2_cars on adv.car equals car.id
                    where (adv.name.ToLower().Contains(search)
                            || car.brand.ToLower().Contains(search)
                           || car.name.ToLower().Contains(search)
                           || car.options.ToString().ToLower().Contains(search))
                    select adv).ToList();
        }

        /// <summary>
        /// Get advertisements & filter them
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static List<Project2_advertisement> GetAdvertisementsByFilter(Dictionary<string, string> filters) {
            db = new AdvertisementsDataClassesDataContext();

            // get all advertisements
            List<Project2_advertisement> advs = db.Project2_advertisements.OrderByDescending(a => a.last_edited).ToList();

            int priceMin = filters.ContainsKey("priceMin") ? Convert.ToInt32(filters["priceMin"]) : 0;
            int priceMax = filters.ContainsKey("priceMax") ? Convert.ToInt32(filters["priceMax"]) : int.MaxValue;
            int mileageMin = filters.ContainsKey("mileageMin") ? Convert.ToInt32(filters["mileageMin"]) : 0;
            int mileageMax = filters.ContainsKey("mileageMax") ? Convert.ToInt32(filters["mileageMax"]) : int.MaxValue;
            int yearMin = filters.ContainsKey("yearMin") ? Convert.ToInt32(filters["yearMin"]) : 0;
            int yearMax = filters.ContainsKey("yearMax") ? Convert.ToInt32(filters["yearMax"]) : int.MaxValue;
            string province = filters.ContainsKey("province") ? filters["province"].ToLower() : "";
            string color = filters.ContainsKey("color") ? filters["color"].ToLower() : "";
            string fuel = filters.ContainsKey("fuel") ? filters["fuel"].ToLower() : "";
            string transmission = filters.ContainsKey("transmission") ? filters["transmission"].ToLower() : "";
            string doors = filters.ContainsKey("doors") ? filters["doors"].ToLower() : "";
            string brand = filters.ContainsKey("brand") ? filters["brand"].ToLower() : "";

            advs = (from adv in advs
                    join car in db.Project2_cars on adv.car equals car.id
                    join usr in db.Project2_users on adv.user_id equals usr.id
                    where
                        (adv.price >= priceMin
                        && adv.price <= priceMax
                        && car.mileage >= mileageMin
                        && car.mileage <= mileageMax
                        && car.year.Year >= yearMin
                        && car.year.Year <= yearMax
                        && usr.province.ToLower().Contains(province)
                        && car.color.ToLower().Contains(color)
                        && car.fuel.ToLower().Contains(fuel)
                        && car.transmission.ToLower().Contains(transmission)
                        && car.doors.ToString().ToLower().Contains(doors)
                        && car.brand.ToLower().Contains(brand)
                        )
                    select adv).OrderByDescending(a => a.last_edited).ToList();

            return advs;
        }

        #endregion

        #region delete

        /// <summary>
        /// Delete an advertisement
        /// </summary>
        public void DeleteAdvertisement() {
            db = new AdvertisementsDataClassesDataContext();

            Project2_advertisement adv = db.Project2_advertisements.Where(a => a.id == id).SingleOrDefault();
            db.Project2_photos.DeleteAllOnSubmit(adv.Project2_photos);
            db.Project2_cars.DeleteOnSubmit(adv.Project2_car);
            db.Project2_advertisements.DeleteOnSubmit(adv);

            db.SubmitChanges();
        }

        #endregion

        #region insert

        /// <summary>
        /// Insert a new advertisement
        /// Save everything in lowercase
        /// </summary>
        /// <param name="model"></param>
        /// <param name="files"></param>
        /// <param name="userId"></param>
        /// <returns>the id of the inserted advertisement</returns>
        public static int InsertAdvertisement(AdvertisementModel model, List<string> files, int userId) {
            db = new AdvertisementsDataClassesDataContext();

            // create all objects first with data from the model
            Project2_car car = new Project2_car();
            car.name = model.carName.ToLower();
            car.brand = model.brand.ToLower();
            car.mileage = model.mileage;
            car.fuel = model.fuel.ToLower();
            car.doors = Convert.ToInt32(model.doors);
            car.color = model.color.ToLower();
            car.transmission = model.transmission.ToLower();
            car.cilinder_capacity = model.cilinder_capacity;
            car.horsepower = model.horsepower;
            car.description = model.description;
            car.options = model.options;
            car.year = new DateTime(model.year, model.month, 1);

            db.Project2_cars.InsertOnSubmit(car);
            db.SubmitChanges();

            Project2_advertisement adv = new Project2_advertisement();
            adv.name = model.name;
            adv.price = model.price;
            adv.user_id = userId;
            adv.last_edited = DateTime.Now;
            adv.car = car.id;

            db.Project2_advertisements.InsertOnSubmit(adv);
            db.SubmitChanges();

            foreach (string filename in files) {
                Project2_photo photo = new Project2_photo();
                photo.advertisement = adv.id;
                photo.name = filename;
                db.Project2_photos.InsertOnSubmit(photo);
            }
            db.SubmitChanges();

            return adv.id;
        }

        /// <summary>
        /// Insert a new offer
        /// </summary>
        /// <param name="model"></param>
        public void NewOffer(OfferModel model) {
            db = new AdvertisementsDataClassesDataContext();

            Project2_offer offer = new Project2_offer();
            offer.advertisement_id = this.id;
            offer.name = model.name;
            offer.price = Convert.ToInt32(model.price);
            offer.email = model.email;
            offer.timestamp = DateTime.Now;
            db.Project2_offers.InsertOnSubmit(offer);
            db.SubmitChanges();
        }

        #endregion

        #region update

        /// <summary>
        /// Update an advertisement
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public void UpdateAdvertisement(AdvertisementModel model, int userId, List<string> files) {
            Project2_advertisement advertisement = db.Project2_advertisements.Where(a => a.id == this.id).SingleOrDefault();
            Project2_car car = advertisement.Project2_car;

            car.name = model.carName.ToLower();
            car.brand = model.brand.ToLower();
            car.mileage = model.mileage;
            car.fuel = model.fuel.ToLower();
            car.doors = Convert.ToInt32(model.doors);
            car.color = model.color.ToLower();
            car.transmission = model.transmission.ToLower();
            car.cilinder_capacity = model.cilinder_capacity;
            car.horsepower = model.horsepower;
            car.description = model.description;
            car.options = model.options;
            car.year = new DateTime(model.year, model.month, 1);

            advertisement.name = model.name;
            advertisement.price = model.price;
            advertisement.last_edited = DateTime.Now;

            if (files.Count > 0) {
                db.Project2_photos.DeleteAllOnSubmit(advertisement.Project2_photos);
                foreach (string filename in files) {
                    Project2_photo photo = new Project2_photo();
                    photo.advertisement = advertisement.id;
                    photo.name = filename;
                    db.Project2_photos.InsertOnSubmit(photo);
                }

            
            }
            db.SubmitChanges();
        }

        #endregion
    }
}