/**
 * Created by Sam on 11/10/13.
 */
$(function () {

    /**
    * ------------------------------------------------------------------------------------------------------
    * Tables
    * ------------------------------------------------------------------------------------------------------
    */

    $("#overview .clickableRow").click(function () {
        window.document.location = $(this).attr("href");
    });

    $("#overview .clickableRow").css('cursor', 'pointer');

    /**
    * ------------------------------------------------------------------------------------------------------
    * Filter and pagination fix
    * ------------------------------------------------------------------------------------------------------
    */

    $('.pager a').on('click', function (e) {
        // when the user clicks on any of the pager links try to extract the page number from the link and set the value of the hidden field
        var page = this.href.match(/page=([0-9])+/)[1];
        console.log(page);

        if ($('#filter').val() == 'filter') {
            // submit the filter form
            $('#page').val(page);
            $('#filterForm').submit();
        } else {
            // submit the search form
            $('#pageSearch').val(page);
            $('#searchForm').submit();
        }

        // cancel the default action of the link which is to simply redirect
        return false;
    });

    /**
    * ------------------------------------------------------------------------------------------------------
    * Lightbox
    * ------------------------------------------------------------------------------------------------------
    */

    $('#open-image').click(function (e) {
        e.preventDefault();
        $(this).ekkoLightbox();
    });

    /**
    * ------------------------------------------------------------------------------------------------------
    * Delete advertisement
    * ------------------------------------------------------------------------------------------------------
    */

    $('.avdDelete').on('click', function (e) {
        window.event.cancelBubble = true;
        return confirm('Bent u zeker dat u deze advertentie wil verwijderen?');
    });

    /**
    * ------------------------------------------------------------------------------------------------------
    * Web API
    * ------------------------------------------------------------------------------------------------------
    */

    var searchTerm = $('#searchTerm').val();
 
    // filter
    if ($('#filter').val() == 'filter') {
        var brand = $('#brand').val();
        var minPrice = $('#priceMax').val();
        var km = $('#mileageMax').val();
        var year = $('#yearMax').val();

        searchTerm += "lessThanPrice=" + minPrice + "&km=" + km + "&year=" + year + "&brand=" + brand;

    // search
    } else {
        if (typeof searchTerm == 'undefined') {
            searchTerm = "search?all";
        } else {
            searchTerm = "searchParam=" + searchTerm;
        }
    }

    $.get('http://arnedeproft.ikdoeict.net/project2api/api/car/search?' + searchTerm, function (data) {
        var counter;
        $.each(data, function (i, item) {
            $('#results').append(
                  '<tr class="clickableRow" href="http://arnedeproft.ikdoeict.net/project2/Cars/Detail?carID=' + this.id + '">' +
                  '<td><a href="http://arnedeproft.ikdoeict.net/project2/Cars/Detail?carID=' + this.id + '""><img src="http://arnedeproft.ikdoeict.net/project2/pictures/' + this.picture + '" class="img-thumbnail" alt="thumbnail"></td>' +
                  '<td>' + this.price + '\u20AC</td>' +
                  '<td>' + this.title + '</td>' +
                  '<td>' + this.address + '</td>' +
                  '<td>' + this.year + '</td>' +
                  '<td>' + this.km + '</td>' +
                  '</tr>'
              );
            if (i == 3) {
                return false;
            }
        });
    });

    $("#overview .clickableRow").click(function () {
        window.document.location = $(this).attr("href");
    });

    $("#overview .clickableRow").css('cursor', 'pointer');
});