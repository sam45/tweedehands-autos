USE [ID141253_samvandenberge1]
GO
/****** Object:  Table [dbo].[Project2_advertisements]    Script Date: 1/16/2014 14:02:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Project2_advertisements](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[price] [int] NOT NULL,
	[last_edited] [datetime] NOT NULL,
	[car] [int] NOT NULL,
	[user_id] [int] NOT NULL,
 CONSTRAINT [PK_Project2_advertisements] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Project2_cars]    Script Date: 1/16/2014 14:02:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Project2_cars](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[mileage] [int] NOT NULL,
	[fuel] [varchar](50) NOT NULL,
	[doors] [int] NOT NULL,
	[color] [varchar](50) NOT NULL,
	[transmission] [varchar](50) NOT NULL,
	[cilinder_capacity] [int] NULL,
	[horsepower] [int] NULL,
	[description] [text] NOT NULL,
	[options] [text] NULL,
	[year] [date] NOT NULL,
	[brand] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Project2_cars] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Project2_offers]    Script Date: 1/16/2014 14:02:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Project2_offers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[price] [int] NOT NULL,
	[timestamp] [datetime] NOT NULL,
	[name] [varchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[advertisement_id] [int] NULL,
 CONSTRAINT [PK_Project2_offers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Project2_photos]    Script Date: 1/16/2014 14:02:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Project2_photos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[advertisement] [int] NOT NULL,
 CONSTRAINT [PK_Project2_photos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Project2_users]    Script Date: 1/16/2014 14:02:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Project2_users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[firstname] [varchar](50) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[password] [varchar](100) NOT NULL,
	[phone] [varchar](50) NULL,
	[address] [varchar](50) NOT NULL,
	[city] [varchar](50) NOT NULL,
	[postal_code] [int] NOT NULL,
	[province] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Project2_users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Project2_advertisements] ON 

INSERT [dbo].[Project2_advertisements] ([id], [name], [price], [last_edited], [car], [user_id]) VALUES (1, N'Audi A2, zo goed als nieuw!', 18250, CAST(0x0000A2B500D07837 AS DateTime), 1, 1)
INSERT [dbo].[Project2_advertisements] ([id], [name], [price], [last_edited], [car], [user_id]) VALUES (3, N'BMW M1, super sportief!', 25000, CAST(0x0000A2A200000000 AS DateTime), 5, 2)
INSERT [dbo].[Project2_advertisements] ([id], [name], [price], [last_edited], [car], [user_id]) VALUES (4, N'Alfa Romeo Mito', 9500, CAST(0x0000A27200000000 AS DateTime), 7, 1)
INSERT [dbo].[Project2_advertisements] ([id], [name], [price], [last_edited], [car], [user_id]) VALUES (5, N'Fiat 500, perfecte staat', 9000, CAST(0x0000A24E00000000 AS DateTime), 8, 1)
INSERT [dbo].[Project2_advertisements] ([id], [name], [price], [last_edited], [car], [user_id]) VALUES (20, N'Aston Martin DB9, ongevalvrij!', 53900, CAST(0x0000A29F00000000 AS DateTime), 22, 2)
INSERT [dbo].[Project2_advertisements] ([id], [name], [price], [last_edited], [car], [user_id]) VALUES (42, N'Citroen C3 Pluriel met keuring', 4900, CAST(0x0000A2A200000000 AS DateTime), 45, 2)
INSERT [dbo].[Project2_advertisements] ([id], [name], [price], [last_edited], [car], [user_id]) VALUES (43, N'Volvo C30 1.6 Turbo - D DRIVe Start/Stop Summum', 15950, CAST(0x0000A2A200000000 AS DateTime), 46, 2)
INSERT [dbo].[Project2_advertisements] ([id], [name], [price], [last_edited], [car], [user_id]) VALUES (44, N'Range Rover classic 300TDi vogue mode', 4800, CAST(0x0000A2A200000000 AS DateTime), 47, 1)
INSERT [dbo].[Project2_advertisements] ([id], [name], [price], [last_edited], [car], [user_id]) VALUES (45, N'volkswagen polo 1.4i edition', 6600, CAST(0x0000A2A200000000 AS DateTime), 48, 1)
INSERT [dbo].[Project2_advertisements] ([id], [name], [price], [last_edited], [car], [user_id]) VALUES (46, N'Lancia Delta 1.6 Multijet Argento', 8749, CAST(0x0000A2A300000000 AS DateTime), 49, 13)
INSERT [dbo].[Project2_advertisements] ([id], [name], [price], [last_edited], [car], [user_id]) VALUES (48, N'Ford Mondeo Ghia 18TDCI', 9500, CAST(0x0000A2A900000000 AS DateTime), 50, 11)
INSERT [dbo].[Project2_advertisements] ([id], [name], [price], [last_edited], [car], [user_id]) VALUES (49, N'Citroën C4 Picasso 1.6 HDI ', 4700, CAST(0x0000A2AD0164228A AS DateTime), 51, 12)
INSERT [dbo].[Project2_advertisements] ([id], [name], [price], [last_edited], [car], [user_id]) VALUES (50, N'Nissan Micra 1.2', 6750, CAST(0x0000A2AD0163A849 AS DateTime), 52, 12)
INSERT [dbo].[Project2_advertisements] ([id], [name], [price], [last_edited], [car], [user_id]) VALUES (51, N'Mercedes 190e oldtimer', 1400, CAST(0x0000A25100000000 AS DateTime), 53, 13)
INSERT [dbo].[Project2_advertisements] ([id], [name], [price], [last_edited], [car], [user_id]) VALUES (52, N'bmw 320d full option', 11800, CAST(0x0000A2A500000000 AS DateTime), 54, 11)
SET IDENTITY_INSERT [dbo].[Project2_advertisements] OFF
SET IDENTITY_INSERT [dbo].[Project2_cars] ON 

INSERT [dbo].[Project2_cars] ([id], [name], [mileage], [fuel], [doors], [color], [transmission], [cilinder_capacity], [horsepower], [description], [options], [year], [brand]) VALUES (1, N'audi a1', 90000, N'diesel', 3, N'wit', N'manueel', 1598, 104, N'Wordt afgeleverd in een onberispelijke staat met:
• Blanco keuringsrapport op méér dan 80 punten !!
• Gedetailleerde onderhoudshistoriek !!
• Car-pass (certificaat kilometerhistoriek)
• Slijtageonderdelen die niet onder de waarborg vallen worden 
vooraleer de wagen vertrekt gecontroleerd, getest en indien nodig
vervangen!', N'Airco, Elektr. ramen v + a, Radio & CD, Airbag dubbel, Airbag enkel, Startonderbreker, Lichtmetalen velgen, Boordcomputer, Parkeersensor, Metallic lak', CAST(0x5D330B00 AS Date), N'audi')
INSERT [dbo].[Project2_cars] ([id], [name], [mileage], [fuel], [doors], [color], [transmission], [cilinder_capacity], [horsepower], [description], [options], [year], [brand]) VALUES (5, N'BMW M1', 36966, N'benzine', 3, N'oranje', N'automatisch', 2979, 340, N'Zwart Boston lederen sportinterieur met oranje contrast. Gegarandeerd ongevalvrij en afkomstig van de 1ste eigenaar ! Belgische wagen ! Deze wagen krijgt een volledige onderhoudsbeurt voor aflevering, wordt keuringsvrij afgeleverd met detailverslag (80 controlepunten + Car-Pass gewaarborgde kilometerstand) ', N'ABS, airbag, airbag lateraal, airbag passagier, airco automatisch, alarm, boordcomputer, centrale vergrendeling, cruise control, electrische ruiten, electrische spiegels, electrische zetels, GSM Bluetooth/carkit, leder, lichtmetalen velgen, mistlampen, navigatiesysteem (GPS), onderhoudsboekje, parkeersensor (PDC), radio/cd, servostuur, stabiliteitscontrole, startonderbreker, tractiecontrole, xenon lichten, zetelverwarming', CAST(0xF2340B00 AS Date), N'BMW')
INSERT [dbo].[Project2_cars] ([id], [name], [mileage], [fuel], [doors], [color], [transmission], [cilinder_capacity], [horsepower], [description], [options], [year], [brand]) VALUES (7, N'Alfa Romeo Mito', 20000, N'diesel', 3, N'rood', N'manueel', 1248, 120, N'Witte Alfa Romeo te koop wegens andere auto', N'Extra uitrusting:
ESP
Klimaatregeling
Laterale airbags
Mistlichten
Roetpartikelfilter', CAST(0x3D360B00 AS Date), N'Alfa Romeo')
INSERT [dbo].[Project2_cars] ([id], [name], [mileage], [fuel], [doors], [color], [transmission], [cilinder_capacity], [horsepower], [description], [options], [year], [brand]) VALUES (8, N'Fiat 500', 25000, N'benzine', 3, N'blauw', N'manueel', 1242, 69, N'Unieke Fiat 500. Twinair+ Sportuitvoering in "Alonso" limited edition versie. Speciale blauwe parelmoerkleur Azzurro midnight met zwart metallic dak. Heeft nog fabrieksgarantie', N'Opties
-airco
-multifunctioneel stuur
-start/stop
-radio/cd met mp3 en aux
-lederen stuur
-blue&me
-half lederen interieur
-electrische ramen
-electrische buitenspiegels
-16 duim alu velgen
-zwart dak origineel
-privacy glas achteraan
-enz', CAST(0x43360B00 AS Date), N'Fiat')
INSERT [dbo].[Project2_cars] ([id], [name], [mileage], [fuel], [doors], [color], [transmission], [cilinder_capacity], [horsepower], [description], [options], [year], [brand]) VALUES (22, N'Aston Martin DB9', 115000, N'benzine', 3, N'grijs', N'automatisch', 5935, 444, N'Ongeval vrij, perfecte staat', N'ABS, Alu velgen, Boordcomputer, Airco, Centrale vergrendeling, Elektrische ruiten, Radio-CD, Cruise control, Airbags, Laterale airbags, Leder, Stuurbekrachtiging (Servo), Navigatiesysteem (GPS), Verwarmde zetels, Tractiecontrole (ASC/ASR/ETC/TCS), Xenon-lichten, Elektrische zetels, Elektronische Stabiliteitsregeling (ESP), Parkeerhulpsysteem (PDS)', CAST(0x042E0B00 AS Date), N'Aston Martin')
INSERT [dbo].[Project2_cars] ([id], [name], [mileage], [fuel], [doors], [color], [transmission], [cilinder_capacity], [horsepower], [description], [options], [year], [brand]) VALUES (45, N'Citroën C3', 133000, N'diesel', 3, N'lightblauw', N'manueel', 1360, 75, N'goede staat 133000 km met keuring en car pass', N'ABS, Stuurbekrachtiging, Lederen bekleding, Airbag dubbel, Airbag enkel, Open dak, Alarm, Startonderbreker, Centr.vergrendeling, Getint glas, Boordcomputer, Elektr.ramen voor, Metallic lak, Elektr. ramen v + a, Radio & CD', CAST(0x6C2E0B00 AS Date), N'Citroën')
INSERT [dbo].[Project2_cars] ([id], [name], [mileage], [fuel], [doors], [color], [transmission], [cilinder_capacity], [horsepower], [description], [options], [year], [brand]) VALUES (46, N'Volvo C30', 40438, N'diesel', 3, N'zilver', N'manueel', 1160, 110, N'High performance audiosysteem, Bluetooth handsfree systeem voor GSM, Verwarmbare zitplaatesen voor, spatlaapen voor en achteraan, Keyless Drive - openen sluiten en starten zonder sluitel, Getinte ruiten achteraan, Parkeerhulpsysteem achteraan, Regensensor, Xenon koplampen met adaptieve bochtverlichting, Mistlampen vooran, 4 somer alloy vlegen met banden en 4 winter alloy vlegen met banden', N'ABS, Alarminstallatie, Armsteun, Alu velgen, Boordcomputer, Airco, Centrale vergrendeling, Elektrische ruiten, Elektrische spiegels, Radio-CD, Cruise control, Luidsprekers, Mistlichten, Airbags, Laterale airbags, Leder, Spoiler, Stuurbekrachtiging (Servo), Gsm-kit, Achterbank 1/3 - 2/3, Navigatiesysteem (GPS), Verwarmde zetels, Getinte ruiten, Houtafwerking, Verlaagde ophanging, Tractiecontrole (ASC/ASR/ETC/TCS), Onderhoudsboekje, Xenon-lichten, FULL OPTION, Radio + CD changer, Multifunctioneel stuur, 1ste eigenaar, Elektrische zetels, Elektronische Stabiliteitsregeling (ESP), Parkeerhulpsysteem (PDS)', CAST(0x50320B00 AS Date), N'Volvo')
INSERT [dbo].[Project2_cars] ([id], [name], [mileage], [fuel], [doors], [color], [transmission], [cilinder_capacity], [horsepower], [description], [options], [year], [brand]) VALUES (47, N'Range Rover', 343000, N'benzine', 5, N'zwart', N'manueel', 1230, 113, N'Deze is in algemene goede staat, maar heeft enkele minpunten :
- achterste uitlaat is stuk
- deuk in de rechter achterflank
- 

Ideaal voor de liefhebber of kenner voor deze jeep met de goede 300TDi motor!!', N'- airco
- interieur in velours met armsteunen
- glazen open dak
- 4x elektrische getinte ramen
- centrale vergrendeling + alarm
- radio/CD kenwood + iPod direct
- originele rubberen voetmatten
- hondenrek achteraan
- mistlichten vooraan
- ijzeren grilles / lichtbeschermers
- afneembare trekhaak (trekvermogen geremd 3010KG)', CAST(0x191E0B00 AS Date), N'Range Rover')
INSERT [dbo].[Project2_cars] ([id], [name], [mileage], [fuel], [doors], [color], [transmission], [cilinder_capacity], [horsepower], [description], [options], [year], [brand]) VALUES (48, N'Volkswagen Polo', 65000, N'benzine', 3, N'zwart', N'manueel', 1390, 75, N'wagen word gekeurd voor verkoop met car-pass en roos 
formulier! 

met 1 jaar garantie en groot onderhoud', N'ABS, Airbag dubbel, Elektr.ramen voor, Radio & CD, Getint glas, Automatische transmissie, Stuurbekrachtiging, Boordcomputer, Lichtmetalen velgen, Centr.vergrendeling, Metallic lak', CAST(0xD42C0B00 AS Date), N'Volkswagen')
INSERT [dbo].[Project2_cars] ([id], [name], [mileage], [fuel], [doors], [color], [transmission], [cilinder_capacity], [horsepower], [description], [options], [year], [brand]) VALUES (49, N'Lancia Delta', 130000, N'diesel', 5, N'wit', N'manueel', 1598, 85, N'Tweedehands in goede staat, btw aftrekbaar', N'Airco, Elektr. ramen v + a, Radio & CD, Airbag dubbel, Airbag enkel, Centr.vergrendeling, Startonderbreker, Boordcomputer', CAST(0x8B300B00 AS Date), N'Lancia')
INSERT [dbo].[Project2_cars] ([id], [name], [mileage], [fuel], [doors], [color], [transmission], [cilinder_capacity], [horsepower], [description], [options], [year], [brand]) VALUES (50, N'ford mondeo', 119000, N'diesel', 5, N'grijs', N'manueel', 1580, 125, N'Zeer mooie niet van nieuw te onderscheiden wagen.', N'Nieuwe banden vooraan.
Start/stop knop,
centrale vergrendeling op afstandsbediening,
elektrische ruiten voor en achter,
elektrische en inklapbare zijspiegels,
automatische klimaatregeling,
automatische licht- regensensoren,
usb aansluiting en multifunctionele stuur,
boordcomputer,
cruise control,
parkeersensoren voor en achter,
led verlichting in de zijspiegels,
lederen interieur,
verwarmde zetels,
afwerking in notenlaar,
afneembare trekhaak,
abs, esp,
10x airbags, etc....', CAST(0x042E0B00 AS Date), N'ford')
INSERT [dbo].[Project2_cars] ([id], [name], [mileage], [fuel], [doors], [color], [transmission], [cilinder_capacity], [horsepower], [description], [options], [year], [brand]) VALUES (51, N'citroën picasso', 220000, N'diesel', 5, N'geel', N'manueel', 1560, 110, N'In perfecte staat ', N'Cruise control, Navigatiesysteem, Elektr. buitenspiegels, Open dak, Elektr. ramen v + a, Parkeersensor, Airco, Radio & CD, Alarm, Startonderbreker, Automatische transmissie, Stuurbekrachtiging, Boordcomputer, Centr.vergrendeling, Metallic lak', CAST(0xEE360B00 AS Date), N'citroën')
INSERT [dbo].[Project2_cars] ([id], [name], [mileage], [fuel], [doors], [color], [transmission], [cilinder_capacity], [horsepower], [description], [options], [year], [brand]) VALUES (52, N'nissan micra', 27000, N'benzine', 5, N'rood', N'manueel', 1110, 95, N'Mooie Nissan Micra 1.2 benzine te koop met maar 27.000 km op de teller. Onderhoudsboekje aanwezig ik ben eerste eigenaar particulier en lever de auto met roze formulier en car pass zoals het hoort Het voertuig heeft enkel 2 krassen 1 op de koffer en 1 op de zijdeur achteraan die zijn er door vandalen opgekrast voor mijn deur. De wagen moet weg wegens aankoop diesel aangezien ik meer km moet gaan rijden voor mijn werk. Momenteel liggen de winterbanden erop als nieuw met de originele stalen velgen maar er zijn ook aluminium velgen bij met zo goed als nieuwe zomerbanden.', N'ABS, Airbag dubbel, Elektr. buitenspiegels, Elektr. ramen v + a, Airco, Radio & CD, Stuurbekrachtiging, Boordcomputer, Lichtmetalen velgen, Centr.vergrendeling', CAST(0x13340B00 AS Date), N'nissan')
INSERT [dbo].[Project2_cars] ([id], [name], [mileage], [fuel], [doors], [color], [transmission], [cilinder_capacity], [horsepower], [description], [options], [year], [brand]) VALUES (53, N'Mercedes 190e', 110000, N'benzine', 5, N'wit', N'manueel', 1150, 102, N'Nette Mercedes oldtimer.', N'ABS, Elektr. buitenspiegels, Open dak, Elektr.ramen voor, Getint glas, Automatische transmissie, Stuurbekrachtiging, Centr.vergrendeling, Metallic lak', CAST(0x0B130B00 AS Date), N'Mercedes')
INSERT [dbo].[Project2_cars] ([id], [name], [mileage], [fuel], [doors], [color], [transmission], [cilinder_capacity], [horsepower], [description], [options], [year], [brand]) VALUES (54, N'BMW 320d', 130000, N'benzine', 5, N'zwart', N'manueel', 1241, 124, N'met onderhoudsboekje
zeer propere wagen
word gekeurd voor verkoop', N'ABS, Cruise control, Navigatiesysteem, Airbag dubbel, Elektr. buitenspiegels, Elektr. ramen v + a, Parkeersensor, Airco, Alarm, Automatische transmissie, Lederen bekleding, Boordcomputer, Lichtmetalen velgen, Centr.vergrendeling, Metallic ', CAST(0xF02F0B00 AS Date), N'BMW')
SET IDENTITY_INSERT [dbo].[Project2_cars] OFF
SET IDENTITY_INSERT [dbo].[Project2_offers] ON 

INSERT [dbo].[Project2_offers] ([id], [price], [timestamp], [name], [email], [advertisement_id]) VALUES (1, 18000, CAST(0x0000A2AD00000000 AS DateTime), N'Arne De Proft', N'arne.deproft@kahosl.be', 20)
INSERT [dbo].[Project2_offers] ([id], [price], [timestamp], [name], [email], [advertisement_id]) VALUES (2, 19000, CAST(0x0000A2AE00000000 AS DateTime), N'Pieter Van Molle', N'pieter.vanmolle@kahosl.be', 20)
INSERT [dbo].[Project2_offers] ([id], [price], [timestamp], [name], [email], [advertisement_id]) VALUES (3, 20000, CAST(0x0000A2A700000000 AS DateTime), N'Arne De Proft', N'arne.deproft@kahosl.be', 20)
INSERT [dbo].[Project2_offers] ([id], [price], [timestamp], [name], [email], [advertisement_id]) VALUES (8, 20500, CAST(0x0000A2A001427787 AS DateTime), N'Sam Van Den Berge', N'van.den.berge.sam@gmail.com', 20)
INSERT [dbo].[Project2_offers] ([id], [price], [timestamp], [name], [email], [advertisement_id]) VALUES (9, 24000, CAST(0x0000A29F00000000 AS DateTime), N'Sam Van Den Berge', N'van.den.berge.sam@gmail.com', 3)
INSERT [dbo].[Project2_offers] ([id], [price], [timestamp], [name], [email], [advertisement_id]) VALUES (10, 12000, CAST(0x0000A2A400B8B08E AS DateTime), N'Arne', N'arne.deproft@kahosl.be', 52)
INSERT [dbo].[Project2_offers] ([id], [price], [timestamp], [name], [email], [advertisement_id]) VALUES (13, 4700, CAST(0x0000A2AE016CBD9B AS DateTime), N'Timon De Cock', N'timon.decock@kahosl.be', 49)
INSERT [dbo].[Project2_offers] ([id], [price], [timestamp], [name], [email], [advertisement_id]) VALUES (14, 18000, CAST(0x0000A2AE016CE58B AS DateTime), N'Julie Van De Putte', N'julievdp@hotmail.com', 1)
INSERT [dbo].[Project2_offers] ([id], [price], [timestamp], [name], [email], [advertisement_id]) VALUES (15, 18500, CAST(0x0000A2AF0156B7A8 AS DateTime), N'Arne De Proft', N'arne.deproft@kahosl.be', 1)
INSERT [dbo].[Project2_offers] ([id], [price], [timestamp], [name], [email], [advertisement_id]) VALUES (16, 17000, CAST(0x0000A2B500D018B0 AS DateTime), N'Rogier', N'rogier.vanderlinde@kahosl.be', 1)
SET IDENTITY_INSERT [dbo].[Project2_offers] OFF
SET IDENTITY_INSERT [dbo].[Project2_photos] ON 

INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (8, N'1.jpg', 3)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (9, N'2.jpg', 3)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (10, N'3.jpg', 3)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (11, N'4.jpg', 3)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (13, N'1.jpg', 4)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (14, N'2.jpg', 4)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (53, N'3.jpg', 4)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (54, N'4.jpg', 4)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (55, N'1.jpg', 5)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (56, N'2.jpg', 5)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (57, N'3.jpg', 5)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (58, N'4.jpg', 5)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (104, N'1.jpg', 42)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (105, N'2.jpg', 42)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (106, N'3.jpg', 42)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (107, N'4.jpg', 42)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (108, N'1.jpg', 43)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (109, N'2.jpg', 43)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (110, N'3.jpg', 43)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (111, N'4.jpg', 43)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (112, N'1.jpg', 44)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (113, N'2.jpg', 44)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (114, N'3.jpg', 44)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (115, N'4.jpg', 44)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (116, N'1.jpg', 45)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (117, N'2.jpg', 45)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (118, N'3.jpg', 45)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (120, N'1.jpg', 46)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (121, N'2.jpg', 46)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (122, N'3.jpg', 46)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (123, N'4.jpg', 46)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (124, N'1.jpg', 50)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (125, N'2.jpg', 50)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (126, N'3.jpg', 50)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (127, N'4.jpg', 50)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (128, N'1.jpg', 51)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (129, N'2.jpg', 51)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (130, N'3.jpg', 51)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (131, N'1.jpg', 52)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (132, N'2.jpg', 52)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (133, N'3.jpg', 52)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (134, N'4.jpg', 52)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (135, N'1.jpg', 49)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (136, N'2.jpg', 49)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (137, N'3.jpg', 49)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (138, N'4.jpg', 49)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (139, N'1.jpg', 48)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (140, N'2.jpg', 48)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (162, N'1.jpg', 1)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (163, N'2.jpg', 1)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (164, N'3.jpg', 1)
INSERT [dbo].[Project2_photos] ([id], [name], [advertisement]) VALUES (165, N'4.jpg', 1)
SET IDENTITY_INSERT [dbo].[Project2_photos] OFF
SET IDENTITY_INSERT [dbo].[Project2_users] ON 

INSERT [dbo].[Project2_users] ([id], [firstname], [name], [email], [password], [phone], [address], [city], [postal_code], [province]) VALUES (1, N'Sam', N'Van Den Berge', N'sam.vandenberge1@kahosl.be', N'L34Ov2IAPkWL2eNd/pOJtx5zNmF0CQE1N9nVH/HG5JY=', N'0496082021', N'Steenweg 68', N'Hundelgem', 9630, N'Oost-Vlaanderen')
INSERT [dbo].[Project2_users] ([id], [firstname], [name], [email], [password], [phone], [address], [city], [postal_code], [province]) VALUES (2, N'Arne', N'De Proft', N'arne.deproft@kahosl.be', N'TjQKahWlohJzpuXMySKQys4Wh6wv54qsHgBR/hwk8rQ=', N'0123456789', N'Baardegemstestraat', N'Baardegem', 9310, N'Limburg')
INSERT [dbo].[Project2_users] ([id], [firstname], [name], [email], [password], [phone], [address], [city], [postal_code], [province]) VALUES (11, N'Yannick', N'Stevens', N'yannick.stevens@kahosl.be', N'oyrFoqw9S5yD8OQ97+M5MBscMvYHgv4/jZ6OoPSmno0=', N'0496021458', N'Velzekestraat', N'Zottegem', 9620, N'Oost-Vlaanderen')
INSERT [dbo].[Project2_users] ([id], [firstname], [name], [email], [password], [phone], [address], [city], [postal_code], [province]) VALUES (12, N'Pieter', N'Van Molle', N'pieter.vanmolle@kahosl.be', N'fKYQcABhHPd76hgGBsAzWDeEMGctg0/EoWde6ZV6p8c=', N'055487744', N'Stationstraat 23', N'Zottegem', 9620, N'Oost-Vlaanderen')
INSERT [dbo].[Project2_users] ([id], [firstname], [name], [email], [password], [phone], [address], [city], [postal_code], [province]) VALUES (13, N'Ibrahim', N'Bulut', N'ibrahim.bulut@kahosl.be', N'CKpY1RAJ7bF7sK0WCUXcIEPyX1whmnfzb2QPtpzIqNg=', N'0458745896', N'Westvlaanderenstraat', N'Brugge', 8000, N'West-Vlaanderen')
SET IDENTITY_INSERT [dbo].[Project2_users] OFF
ALTER TABLE [dbo].[Project2_advertisements]  WITH CHECK ADD  CONSTRAINT [FK_Project2_advertisements_Project2_cars] FOREIGN KEY([car])
REFERENCES [dbo].[Project2_cars] ([id])
GO
ALTER TABLE [dbo].[Project2_advertisements] CHECK CONSTRAINT [FK_Project2_advertisements_Project2_cars]
GO
ALTER TABLE [dbo].[Project2_advertisements]  WITH CHECK ADD  CONSTRAINT [FK_Project2_advertisements_Project2_users] FOREIGN KEY([user_id])
REFERENCES [dbo].[Project2_users] ([id])
GO
ALTER TABLE [dbo].[Project2_advertisements] CHECK CONSTRAINT [FK_Project2_advertisements_Project2_users]
GO
ALTER TABLE [dbo].[Project2_offers]  WITH CHECK ADD  CONSTRAINT [FK_Project2_offers_Project2_advertisements] FOREIGN KEY([advertisement_id])
REFERENCES [dbo].[Project2_advertisements] ([id])
GO
ALTER TABLE [dbo].[Project2_offers] CHECK CONSTRAINT [FK_Project2_offers_Project2_advertisements]
GO
ALTER TABLE [dbo].[Project2_photos]  WITH CHECK ADD  CONSTRAINT [FK_Project2_photos_Project2_advertisements] FOREIGN KEY([advertisement])
REFERENCES [dbo].[Project2_advertisements] ([id])
GO
ALTER TABLE [dbo].[Project2_photos] CHECK CONSTRAINT [FK_Project2_photos_Project2_advertisements]
GO
